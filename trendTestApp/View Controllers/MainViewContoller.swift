//
//  MainViewContoller.swift
//  trendTestApp
//
//  Created by dingo on 14.03.2018.
//  Copyright © 2018 creatif. All rights reserved.
//

import UIKit

enum Sort {
    case price
    case district
    case metro
    case deadline
}

class MainViewController : UIViewController {
    var delegate : MainViewContollerDelegate!

    // SORT
    var sortSelectViewYPosition = CGFloat()
    @IBOutlet weak var sortSelectView: UIScrollView!
    @IBOutlet weak var totalBouldCountLabel: UILabel!
    @IBOutlet weak var sortByPriceBtn: UIButton!
    @IBOutlet weak var sortByDistrictBtn: UIButton!
    @IBOutlet weak var sortBySubwayBtn: UIButton!
    @IBOutlet weak var sortByDeadlineBtn: UIButton!
    
    @IBOutlet weak var currentSortIndicator: UIView!
    @IBOutlet weak var sortIndicatorLeftConstraint: NSLayoutConstraint!
    @IBOutlet weak var sortIndicatorRightConstraint: NSLayoutConstraint!
    
    
    
    @IBOutlet weak var headerBackground: UIView!

    //DROP DOWN
    var priceSelectViewYPosition = CGFloat()
    @IBOutlet weak var priceSelectView: UIStackView!
    let stepCount = 500000
    let defaultMinPrice = 1000000
    var priceFrom : Int = 0 // лямон
    var priceTo : Int = Int.max
    
    let PRICE_FROM_SELECTOR_TAG = 1
    let PRICE_TO_SELECTOR_TAG = 2
    let SORT_SCROLL_VIEW_TAG = 3

    private var priceFromSelectTableView: UITableView!
    private var priceToSelectTableView: UITableView!
    
    @IBOutlet weak var priceFromView: UIView!
    @IBOutlet weak var priceToView: UIView!
    
    @IBOutlet weak var priceFromLabel: UILabel!
    @IBOutlet weak var priceToLabel: UILabel!
    
    @IBOutlet weak var priceFromArrow: UIImageView!
    @IBOutlet weak var priceToArrow: UIImageView!
    
    
    let priceFromText = "Цена от, руб"
    let priceToText = "Цена до, руб"
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        priceFrom = defaultMinPrice
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        //Установка бэкграунда для хедера
        let gradient = CAGradientLayer()
        gradient.startPoint = CGPoint(x: 0.2, y: 0.0);
        gradient.endPoint = CGPoint(x: 1.0,y: 0.9);
        gradient.colors = [UIColor.init(red: 121/255.0, green: 0/255.0, blue: 111/255.0, alpha: 1.0).cgColor,
                           UIColor.init(red: 99/255.0, green: 0/255.0, blue: 125/255.0, alpha: 1.0).cgColor]
        gradient.frame = headerBackground.frame
        headerBackground.layer.insertSublayer(gradient, at: 0)
        
        initPriceSelectors()
        initSortView()
    }
    
    
    @IBAction func showPriceFrom(_ sender: Any) {
        if priceFromSelectTableView.alpha == 1.0 {
            UIView.animate(withDuration: 0.2, animations: {
                self.priceFromSelectTableView.alpha = 0.0
                self.priceFromArrow.transform = CGAffineTransform(rotationAngle: CGFloat.pi * 2)
            })
        }else{
            UIView.animate(withDuration: 0.2, animations: {
                self.priceFromSelectTableView.alpha = 1.0
                self.priceFromArrow.transform = CGAffineTransform(rotationAngle: CGFloat.pi)
            })
        }
        
        
    }
    
    @IBAction func showPriceTo(_ sender: Any) {
        if priceToSelectTableView.alpha == 1.0 {
            UIView.animate(withDuration: 0.1, animations: {
                self.priceToSelectTableView.alpha = 0.0
                self.priceToArrow.transform = CGAffineTransform(rotationAngle: CGFloat.pi * 2)
            })
        }else{
            UIView.animate(withDuration: 0.1, animations: {
                self.priceToSelectTableView.alpha = 1.0
                self.priceToArrow.transform = CGAffineTransform(rotationAngle: CGFloat.pi)
            })
        }
    }
    
    
    func initPriceSelectors(){
        //Берем данные экрана
        let displayWidth: CGFloat = self.view.frame.width
        let displayHeight: CGFloat = self.view.frame.height
        
        
        let nib = UINib(nibName: "priceCell", bundle: nil)
        priceSelectViewYPosition = self.priceSelectView.frame.origin.y

        
        // Установка координатов
        priceFromSelectTableView = UITableView(frame: CGRect(x: 0,
                                                             y: priceFromView.frame.origin.y +
                                                                priceFromView.bounds.height +
                                                                headerBackground.bounds.height,
                                                             width: (displayWidth / 2) - 2,
                                                             height: displayHeight / 2))
        
        // Маленькие штрихи
        priceFromSelectTableView.register(nib, forCellReuseIdentifier: "PriceSelectTableViewCell")
        priceFromSelectTableView.dataSource = self
        priceFromSelectTableView.delegate = self
        priceFromSelectTableView.alpha = 0.0
        priceFromSelectTableView.rowHeight = 60.0
        
        // Установка тэга по которому будем распозновать
        priceFromSelectTableView.tag = PRICE_FROM_SELECTOR_TAG
        
        // Добавляем в View
        self.view.addSubview(priceFromSelectTableView)
        
        // Установка координатов
        priceToSelectTableView = UITableView(frame: CGRect(x: priceToView.frame.origin.x,
                                                           y: priceFromView.frame.origin.y +
                                                            priceFromView.bounds.height +
                                                            headerBackground.bounds.height,
                                                           width: (displayWidth / 2) - 2,
                                                           height: displayHeight / 2))
        // Маленькие штрихи
        priceToSelectTableView.register(nib, forCellReuseIdentifier: "PriceSelectTableViewCell")
        priceToSelectTableView.dataSource = self
        priceToSelectTableView.delegate = self
        priceToSelectTableView.alpha = 0.0
        priceToSelectTableView.rowHeight = 60.0
        
        // Установка тэга по которому будем распозновать
        priceToSelectTableView.tag = PRICE_TO_SELECTOR_TAG
        
        // Добавляем в View
        self.view.addSubview(priceToSelectTableView)
        
    }
    
    func initSortView(){
        sortSelectView.delegate = self
        sortSelectView.tag = SORT_SCROLL_VIEW_TAG
        sortSelectViewYPosition = sortSelectView.frame.origin.y

        //Устанавливаем ползунок на сортировку
        
        setSortIndicator(.price)
    }
    
    func setSortIndicator(_ sort : Sort){
        
        delegate.sort(sortType: sort)
        
        var x1 : CGFloat = 0.0
        var x2 : CGFloat = 0.0
        
        switch sort {
        case .price:
            
            x1 = sortByPriceBtn.frame.origin.x
            x2 = sortByPriceBtn.frame.origin.x + sortByPriceBtn.frame.width
            
            break
        case .district :
            
            x1 = sortByDistrictBtn.frame.origin.x
            x2 = sortByDistrictBtn.frame.origin.x + sortByDistrictBtn.frame.width
            
            break
        case .metro :
            
            x1 = sortBySubwayBtn.frame.origin.x
            x2 = sortBySubwayBtn.frame.origin.x + sortBySubwayBtn.frame.width
            
            break
        case .deadline :
            
            x1 = sortByDeadlineBtn.frame.origin.x
            x2 = sortByDeadlineBtn.frame.origin.x + sortByDeadlineBtn.frame.width
            
            break
        }
        
        
        UIView.animate(withDuration: 0.1, animations: {
            self.currentSortIndicator.frame = CGRect(x: x1, y: self.currentSortIndicator.frame.origin.y, width: x2 - x1, height: self.currentSortIndicator.frame.height)
        })
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "TableViewControllerSegue") {
            let tableViewController = segue.destination as! TableViewController
            self.delegate = tableViewController
            tableViewController.delegate = self
        }
    }
    
    
    //Sort
    @IBAction func sortByPriceAction(_ sender: Any) {
        setSortIndicator(.price)
    }
    
    @IBAction func sortByDistrictAction(_ sender: Any) {
        setSortIndicator(.district)
    }
    
    @IBAction func sortBySubwayAction(_ sender: Any) {
        setSortIndicator(.metro)
    }
    
    @IBAction func sortByDeadlineAction(_ sender: Any) {
        setSortIndicator(.deadline)
    }
}
extension MainViewController : UITableViewDelegate, UITableViewDataSource {
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (tableView.tag == PRICE_FROM_SELECTOR_TAG){
            //Это таблица выбора цены от
            return priceFrom == defaultMinPrice ? 20 : (20 - 1)
        }else {
            //Это таблица выбора цены до
            return 20
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if (tableView.tag == PRICE_FROM_SELECTOR_TAG){
            //Цена от
            let cell = tableView.dequeueReusableCell(withIdentifier: "PriceSelectTableViewCell", for: indexPath) as! PriceSelectTableViewCell
            
            //проверка если определен цена от
            if (indexPath.row == 0){
                cell.priceLabel.text = "Сбросить"
                cell.selectedArrow.isHidden = true
                cell.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            }else{
                //Цены начинаются от лямона
                let price = (indexPath.row + 1) * stepCount
                
                cell.priceLabel.text = customFormattedAmountString(amount: price)
                
                if (priceFrom == price){
                    //Устанавливаем галочку
                    cell.selectedArrow.isHidden = false
                    cell.backgroundColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
                }else{
                    cell.selectedArrow.isHidden = true
                    cell.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                }
            }
            
            return cell
        }else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "PriceSelectTableViewCell", for: indexPath) as! PriceSelectTableViewCell
            
            //проверка если определен цена от
            if (indexPath.row == 0){
                cell.priceLabel.text = "Сбросить"
                cell.selectedArrow.isHidden = true
                cell.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            }else{
                //Цены начинаются от лямона
                let price = (indexPath.row + 1 + ((priceFrom - 1) / stepCount)) * stepCount
                
                cell.priceLabel.text = customFormattedAmountString(amount: price)
                
                if (priceTo == price){
                    //Устанавливаем галочку
                    cell.selectedArrow.isHidden = false
                    cell.backgroundColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
                }else{
                    cell.selectedArrow.isHidden = true
                    cell.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                }
                
            }
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch tableView.tag {
        case PRICE_FROM_SELECTOR_TAG:
            // берем цену
            if (indexPath.row == 0){
                //Это кнопка сбросить
                priceFrom = defaultMinPrice
                
                setFromTextPriceLabel(defaultMinPrice, true)
                
            } else {
                let price = (indexPath.row + 1) * stepCount
                priceFrom = price
                
                if price >= priceTo {
                    priceTo = price + stepCount
                    
                    setToTextPriceLabel(priceTo, false)
                    
                }
                
                setFromTextPriceLabel(price, false)
                
            }
            
            self.priceFromSelectTableView.reloadData()
            self.priceToSelectTableView.reloadData()
            setPriceInterval()
            break
        case PRICE_TO_SELECTOR_TAG:
            if (indexPath.row == 0){
                //Это кнопка сбросить
                priceTo = Int.max
                setToTextPriceLabel(0, true)
            } else {
                let price = (indexPath.row + 1 + ((priceFrom - 1) / stepCount)) * stepCount
                priceTo = price
                
                setToTextPriceLabel(price, false)
            }
            self.priceToSelectTableView.reloadData()
            setPriceInterval()
            break
        default:
            break
        }
    }
    
    func setFromTextPriceLabel(_ price : Int, _ isMinimal : Bool){
        priceFromLabel.text = isMinimal ? priceFromText : customFormattedAmountString(amount: price)! + " руб."
        priceFromLabel.textColor = isMinimal ? UIColor.lightGray : UIColor.black
    }
    
    func setToTextPriceLabel(_ price : Int, _ isMinimal : Bool){
        priceToLabel.text = isMinimal ? priceToText : customFormattedAmountString(amount: price)! + " руб."
        priceToLabel.textColor = isMinimal ? UIColor.lightGray : UIColor.black
    }
    
    func setPriceInterval(){
        delegate.priceFromTo(self.priceFrom, self.priceTo)
    }
}

extension MainViewController : UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        if (scrollView.tag == SORT_SCROLL_VIEW_TAG){
            scrollView.contentOffset.y = 0.0
        }
        
        
    }
    
}

extension MainViewController : mainTableScrollDelegate {

    func scrolling(distance: CGFloat) {
        /*
        if (distance <= 50){
            // перемещаю на вверх scroll view
            
            let x = sortSelectView.frame.origin.x
            
            let height = sortSelectView.frame.size.height
            let width = sortSelectView.frame.size.width
            
            UIView.animate(withDuration: 0.1, animations: {
                self.sortSelectView.frame = CGRect(x: x, y: self.sortSelectViewYPosition - distance, width: width, height: height)
            })
            
        }else if (distance <= 100){
            // перемещаю на вверх price select view || scroll view
            
            let x = priceSelectView.frame.origin.x
            
            let height = priceSelectView.frame.size.height
            let width = priceSelectView.frame.size.width
            
            
            let sortSelectViewX = sortSelectView.frame.origin.x
            
            let sortSelectViewHeight = sortSelectView.frame.size.height
            let sortSelectViewWidth = sortSelectView.frame.size.width
            
            UIView.animate(withDuration: 0.1, animations: {
                self.sortSelectView.frame = CGRect(x: sortSelectViewX, y: self.sortSelectViewYPosition - distance, width: sortSelectViewWidth, height: sortSelectViewHeight)
                self.priceSelectView.frame = CGRect(x: x, y: self.priceSelectViewYPosition - distance, width: width, height: height)
            })
            
            UIView.animate(withDuration: 0.1, animations: {
                
            })
        }
 
        print(distance)
 
         */
    }
    
    func totalBuildCount(count : Int){
        self.totalBouldCountLabel.text = "\(count) объектов"
    }

}

