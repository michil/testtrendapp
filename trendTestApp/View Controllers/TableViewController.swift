//
//  ViewController.swift
//  trendTestApp
//
//  Created by dingo on 13.03.2018.
//  Copyright © 2018 creatif. All rights reserved.
//

import UIKit
import SwiftyJSON
import AlamofireImage

class TableViewController: UITableViewController, MainViewContollerDelegate {
    
    var delegate : mainTableScrollDelegate!
    
    @IBOutlet weak var loadMoreButtonView: UIView!
    @IBOutlet weak var loadMoreIndicator: UIActivityIndicatorView!
    
    
    //Инициализация API
    let api = API()
    
    //Объявляем массив домов
    var builds = [Building]()
    
    // Текущее смещение
    var CURRENT_OFFSET = 0
    
    // Цены от
    var priceFrom : Int = 0
    var priceTo : Int = Int.max
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Настройка load more button view
        loadMoreButtonView.layer.cornerRadius = 5.0
        loadMoreButtonView.layer.masksToBounds = true
        loadMoreButtonView.layer.borderColor = #colorLiteral(red: 0.6, green: 0.6, blue: 0.6, alpha: 1)
        loadMoreButtonView.layer.borderWidth = 1.0
        
        loadMore(false)
    }
    
    @IBAction func loadMoreButtonAction(_ sender: Any) {
        loadMore(false)
    }
    
    func loadMore(_ reload : Bool){
        if reload {
            CURRENT_OFFSET = 0
            self.builds.removeAll()
        }
        
        if CURRENT_OFFSET == 0 {
            // Показать загрузку на всю таблицу
            
        }else{
            // Показать индикатор Load More
            loadMoreIndicator.isHidden = false
        }
        
        CURRENT_OFFSET += 1
        
        api.getBuildings(page: CURRENT_OFFSET, priceFrom: priceFrom, priceTo: priceTo, completion: { result in
            
            for index in 0..<result["data"]["results"].arrayValue.count {
                
                let buildJson = result["data"]["results"][index]
                
                var building = Building()
                
                building.name = buildJson["name"].stringValue
                building.image = buildJson["image"].stringValue
                building.deadline = buildJson["deadline"].stringValue
                building.region = buildJson["address"].stringValue
                building.builder = buildJson["builder"]["name"].stringValue
                
                for subwayIndex in 0..<buildJson["subways"].arrayValue.count {
                    let subwayJson = buildJson["subways"][subwayIndex]
                    
                    var subway = Subway()
                    
                    subway.logo = ""
                    subway.name = subwayJson["name"].stringValue
                    subway.transportDistance = subwayJson["distance_timing"].intValue
                    
                    building.subway.append(subway)
                }
                
                for pricingIndex in 0..<buildJson["min_prices"].arrayValue.count {
                    let pricingJson = buildJson["min_prices"][pricingIndex]
                    
                    var pricing = BuildPricing()
                    
                    pricing.price = pricingJson["price"].intValue
                    pricing.rooms = pricingJson["rooms"].stringValue
                    
                    building.pricing.append(pricing)
                }
                
                self.builds.append(building)
                self.tableView.reloadData()
                
                self.loadMoreIndicator.isHidden = true
                
                self.delegate.totalBuildCount(count: self.builds.count)
                
            }
        })
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return builds.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "house_cell") as! BuildTableViewCell
        
        let build = self.builds[indexPath.row]
        
        cell.buildName.text = build.name
        cell.buildDeadline.text = build.deadline
        cell.buildDistrict.text = build.region
        cell.buildBuilder.text = build.builder
        
        if build.subway.count != 0 {
            cell.buildSubway.text = build.subway[0].name
            cell.buildTransportTime.text = String(build.subway[0].transportDistance) + " мин."
        }else{
            cell.buildSubway.text = "Рядом отсутствуют"
        }
        
        var rooms = ""
        var roomPrices = ""
        
        for index in 0..<build.pricing.count {
            let pricing = build.pricing[index]
            
            rooms += pricing.rooms
            roomPrices += customFormattedAmountString(amount: pricing.price)! + " руб."
            
            if (index != build.pricing.count - 1){
                rooms += "\n"
                roomPrices += "\n"
            }
        }
        
        cell.buildRoom.text = rooms
        cell.buildRoomPrice.text = roomPrices
        
        DispatchQueue.main.async {
            if let url = URL(string: build.image){
                cell.buildimage.af_setImage(withURL: url)
            }
        }
        
        return cell
    }
    
    
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        delegate.scrolling(distance: scrollView.contentOffset.y)
    }
    
    func sort(sortType: Sort) {
        
        switch sortType {
        case .price:
            builds.sort { $0.pricing[0].price < $1.pricing[0].price }
            
            break
            
        case .deadline:
            builds.sort { $0.deadline < $1.deadline }
            
            break
        case .district:
            builds.sort { $0.region < $1.region }

            break
        case .metro:
            builds.sort { $0.subway[0].transportDistance < $1.subway[0].transportDistance }

            break
        }
        
        self.tableView.reloadData()
        
        
        
    }
    
    func priceFromTo(_ from: Int, _ to: Int) {
        self.priceFrom = from
        self.priceTo = to

        
        loadMore(true)
        
    }
    
}

