//
//  Protocols.swift
//  trendTestApp
//
//  Created by dingo on 14.03.2018.
//  Copyright © 2018 creatif. All rights reserved.
//

import Foundation
import UIKit

protocol mainTableScrollDelegate {
    func scrolling(distance : CGFloat)
    func totalBuildCount(count : Int)
}

protocol MainViewContollerDelegate {
    func sort(sortType : Sort)
    func priceFromTo( _ from : Int, _ to : Int)
}
