//
//  Constants.swift
//  trendTestApp
//
//  Created by dingo on 14.03.2018.
//  Copyright © 2018 creatif. All rights reserved.
//

import Foundation


public let serverURL = "http://api.trend-dev.ru/"
public let serverApiURL = serverURL + "v3_1/"
