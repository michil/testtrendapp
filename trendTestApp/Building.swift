//
//  Building.swift
//  trendTestApp
//
//  Created by dingo on 14.03.2018.
//  Copyright © 2018 creatif. All rights reserved.
//

import Foundation

struct Building {
    var name : String
    var deadline : String
    var image : String
    var region : String
    var builder : String
    var pricing : [BuildPricing]
    var subway : [Subway]
    
    init() {
        self.name = ""
        self.deadline = ""
        self.image = ""
        self.region = ""
        self.builder = ""
        self.pricing = [BuildPricing]()
        self.subway = [Subway]()
    }
}

struct BuildPricing {
    var rooms : String
    var price : Int
    
    init() {
        self.rooms = ""
        self.price = 0
    }
}
struct Subway {
    var logo : String
    var name : String
    var transportDistance : Int
    
    init() {
        self.logo = ""
        self.name = ""
        self.transportDistance = 0
    }
}

