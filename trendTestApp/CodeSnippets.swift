//
//  CodeSnippets.swift
//  trendTestApp
//
//  Created by dingo on 14.03.2018.
//  Copyright © 2018 creatif. All rights reserved.
//
import Foundation


// FROM :: https://stackoverflow.com/a/44363562/6085536
func customFormattedAmountString(amount: Int) -> String? {
    let formatter = NumberFormatter()
    formatter.numberStyle = .decimal
    formatter.locale = Locale.init(identifier: "ru_RU")
    
    if let amountString = formatter.string(from: NSNumber(value: amount)) {
        // check if it has default space like EUR
        let hasSpace = amountString.rangeOfCharacter(from: .whitespaces) != nil
        
        if let indexOfSymbol = amountString.index(of: Character(formatter.currencySymbol)) {
            if amountString.startIndex == indexOfSymbol {
                formatter.paddingPosition = .afterPrefix
            } else {
                formatter.paddingPosition = .beforeSuffix
            }
        }
        if !hasSpace {
            formatter.formatWidth = amountString.count + 1
            formatter.paddingCharacter = " "
        }
    } else {
        print("Error while making amount string from given amount: \(amount)")
        return nil
    }
    
    if let finalAmountString = formatter.string(from: NSNumber(value: amount)) {
        return finalAmountString
    } else {
        return nil
    }
}
