//
//  PriceSelectTableViewCell.swift
//  trendTestApp
//
//  Created by dingo on 14.03.2018.
//  Copyright © 2018 creatif. All rights reserved.
//

import UIKit

class PriceSelectTableViewCell: UITableViewCell {

    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var selectedArrow: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
