//
//  BuildTableViewCell.swift
//  trendTestApp
//
//  Created by dingo on 14.03.2018.
//  Copyright © 2018 creatif. All rights reserved.
//

import UIKit

class BuildTableViewCell: UITableViewCell {

    @IBOutlet weak var cellView: UIView!
    @IBOutlet weak var buildName: UILabel!
    @IBOutlet weak var buildDeadline: UILabel!
    @IBOutlet weak var buildimage: UIImageView!
    @IBOutlet weak var buildDistrict: UILabel!
    @IBOutlet weak var buildBuilder: UILabel!
    @IBOutlet weak var buildSubway: UILabel!
    @IBOutlet weak var buildTransportTime: UILabel!
    
    @IBOutlet weak var buildRoom: UILabel!
    @IBOutlet weak var buildRoomPrice: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        cellView.layer.cornerRadius = 5.0
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
