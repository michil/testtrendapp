//
//  API.swift
//  trendTestApp
//
//  Created by dingo on 14.03.2018.
//  Copyright © 2018 creatif. All rights reserved.
//

import Alamofire
import SwiftyJSON

class API {
    
    init() {
        
    }
    
    func getBuildings(page : Int, priceFrom : Int, priceTo : Int, completion: @escaping (_ result : JSON) -> Void) {
        
        Alamofire.request(serverApiURL + "blocks/search/?show_type=list&count=10&offset=\(page)&cache=false&price_from=\(priceFrom)&price_to=\(priceTo)", method : .get).responseJSON { response in
            
            if let result = response.result.value {
                let json = JSON(result)
                
                completion(json)
                
            }
        }
        
    }
    
}
